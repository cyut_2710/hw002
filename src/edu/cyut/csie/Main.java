package edu.cyut.csie;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Scanner scanner=new Scanner(System.in);
        int grade=scanner.nextInt();
        switch (grade/10)
        {
            case 10:
                System.out.println("得A");
                break;
            case 9:
                System.out.println("得A");
                break;
            case 8:
                System.out.println("得B");
                break;
            case 7:
                System.out.println("得C");
                break;
            case 6:
                System.out.println("得D");
                break;
            case 5:
                System.out.println("得E");
                break;
            case 4:
                System.out.println("得E");
                break;
            case 3:
                System.out.println("得E");
                break;
            case 2:
                System.out.println("得E");
                break;
            case 1:
                System.out.println("得E");
                break;
        }
    }
}
